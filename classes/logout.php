<?php

if ( !class_exists( 'StardustSubscribersLogout' ) ) {
    class StardustSubscribersLogout {
        function __construct() {
            add_filter( 'logout_redirect', array( $this, 'logout_redirect' ), 10, 3 );
        }

        public function logout_redirect( $redirect_to, $request, $user ) {
            if ( isset($user->roles) && is_array( $user->roles ) ) {
                if ( in_array( 'subscriber', $user->roles ) ) {
                    return home_url( );
                }
            }
            return $redirect_to;
        }
    }
}
