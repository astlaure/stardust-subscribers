<?php

if ( !class_exists( 'StardustSubscribersLogin' ) ) {
    class StardustSubscribersLogin {
        function __construct() {
            add_action( 'wp_login_failed', array( $this, 'login_failed' ) );
            add_filter('authenticate', array( $this, 'login_missing' ), 10, 3);
            add_filter( 'login_redirect', array( $this, 'login_redirect' ), 10, 3 );
        }

        public function login_failed( $username ) {
            $referrer = wp_get_referer();

            if (!empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin')) {
                $url = add_query_arg('login', 'failed', site_url('login'));
                wp_safe_redirect($url);
                exit;
            }
        }

        public function login_missing($user, $username, $password) {
            $referer = wp_get_referer();

            if (empty($referer) || strstr($referer, 'wp-login') || strstr($referer, 'wp-admin')) {
                return $user;
            }

            if ( empty(trim($username)) || empty(trim($password)) ) {
                $url = add_query_arg('login', 'failed', site_url('login'));
                wp_safe_redirect($url);
                exit();
            }

            return $user;
        }

        public function login_redirect( $redirect_to, $request, $user ) {
            if ( isset($user->roles) && is_array( $user->roles ) ) {
                if ( in_array( 'subscriber', $user->roles ) ) {
                    return home_url( );
                }
            }
            return $redirect_to;
        }
    }
}
