<?php

if ( !class_exists( 'StardustPluginSubscribersLoginForm' ) ) {
    class StardustPluginSubscribersLoginForm {
        function __construct() {
            add_shortcode( 'stardust_login_form', array( $this, 'add_shortcode' ) );
        }

        public function add_shortcode( $atts = array(), $content = null, $tag = '' ) {
            if ( ! is_user_logged_in() ) { // Display WordPress login form:
                $args = array(
                    'redirect' => admin_url(),
                    'form_id' => 'loginform-custom',
                    'label_username' => __( 'Username' ),
                    'label_password' => __( 'Password' ),
                    'label_remember' => __( 'Remember Me' ),
                    'label_log_in' => __( 'Log In' ),
                    'remember' => true
                );
                if ($_GET['login'] == 'failed') :?>
                    <p class="login-error"><?php _e('Wrong credentials.', 'stardust-plugin'); ?></p>
                <?php endif;
                wp_login_form( $args );
            } else { // If logged in:
                wp_loginout( home_url() ); // Display "Log Out" link.
//	echo " | ";
//	wp_register('', ''); // Display "Site Admin" link.
            }
        }
    }
}
