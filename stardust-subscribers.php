<?php
/*
Plugin Name: Stardust Subscribers
Description: Subscribers Plugin for the Stardust Ecosystem. Provides a more secured login error message and protect the rest api.
Version: 1.0.0
Author Name: Alexandre St-Laurent
Author URI: https://github.com/astlaure
*/

if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( !class_exists( 'StardustSubscribers' ) ) {
    class StardustSubscribers {
        function __construct() {
            add_action( 'init', array( $this, 'register_protected_field' ) );
            add_action( 'init', array( $this, 'protected_check' ));
            add_action( 'wp_head', array( $this, 'hide_adminbar' ) );

            add_action( 'init', array( $this, 'gutenberg_register_scripts' ) );
            add_action( 'enqueue_block_editor_assets', array( $this, 'gutenberg_enqueue_scripts' ) );

            require_once __DIR__ . '/classes/login.php';
            require_once __DIR__ . '/classes/logout.php';

            new StardustSubscribersLogin();
            new StardustSubscribersLogout();
        }

        public function register_protected_field() {
            $screens = array_merge([ 'page', 'post' ]);
            foreach ( $screens as $screen ) {
                register_post_meta($screen, 'protected', array(
                    'type' => 'boolean',
                    'single' => true,
                    'show_in_rest' => true,
                ));
            }
        }

        public function protected_check() {
            $id = url_to_postid( $_SERVER['REQUEST_URI'] , '_wpg_def_keyword', true );
            $protected = get_post_meta($id, 'protected', true);

            if ($protected && !is_user_logged_in()) {
                wp_safe_redirect('/login');
                exit();
            }
        }

        public function hide_adminbar() {
            if (current_user_can('subscriber')) {
                add_filter('show_admin_bar','__return_false');
            }
        }

        public function gutenberg_register_scripts() {
            $asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php');

            wp_register_script(
                'stardust-subscribers-editor',
                plugins_url( 'build/index.js', __FILE__ ),
                $asset_file['dependencies'],
                $asset_file['version']
            );
        }

        public function gutenberg_enqueue_scripts() {
            if ( get_current_screen()->id === 'widgets' ) {
                return;
            }

            wp_enqueue_script( 'stardust-subscribers-editor' );
        }

        static public function activate() {
            require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

            if ( !is_plugin_active( 'stardust-core/stardust-core.php' ) ) {
                ?>
                <div class="notice notice-error" >
                    <p>Please enable Stardust Core before activating Stardust I18n</p>
                </div>
                <?php
                @trigger_error(__( 'Please enable Stardust Core before activating Stardust I18n', 'stardust-i18n' ), E_USER_ERROR);                
            }
            
            update_option( 'rewrite_rules', '' );

            $role = get_role('subscriber');
            if ($role->has_cap('read')) {
                $role->remove_cap('read');
            }
        }

        static public function deactivate() {
            flush_rewrite_rules();

            $role = get_role('subscriber');
            if (!$role->has_cap('read')) {
                $role->add_cap('read');
            }
        }
        
        static public function uninstall() {}
    }
}

if ( class_exists( 'StardustSubscribers' ) ) {
    register_activation_hook( __FILE__, array( 'StardustSubscribers', 'activate' ) );
	register_deactivation_hook( __FILE__, array( 'StardustSubscribers', 'deactivate' ) );
	register_uninstall_hook( __FILE__, array( 'StardustSubscribers', 'uninstall' ) );

    new StardustSubscribers();
}
