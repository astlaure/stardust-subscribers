import './validator';
import { registerPlugin } from "@wordpress/plugins";
import StardustPanel from './stardust-panel';

registerPlugin( 'stardust-subscribers', {
  render: StardustPanel,
} );
