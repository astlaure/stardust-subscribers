import { compose } from '@wordpress/compose';
import { CheckboxControl, PanelRow } from '@wordpress/components';
import { withDispatch, withSelect } from "@wordpress/data";

const screens = ['page', 'post'];

const ProtectedField = ({ postType, postMeta, setPostMeta }) => {
  // Initial setup
  if ( !screens.includes(postType) ) return null;

  return (
    <PanelRow>
      <CheckboxControl
        label="Protected"
        checked={postMeta.protected}
        onChange={( value ) => setPostMeta({...postMeta, protected: value})}
      />
    </PanelRow>
  )
};

export default compose( [
  withSelect( ( select ) => {
    return {
      postMeta: select( 'core/editor' ).getEditedPostAttribute( 'meta' ),
      postType: select( 'core/editor' ).getCurrentPostType(),
    };
  } ),
  withDispatch( ( dispatch ) => {
    return {
      setPostMeta( newMeta ) {
        dispatch( 'core/editor' ).editPost( { meta: newMeta } );
      },
    };
  } )
] )( ProtectedField );
