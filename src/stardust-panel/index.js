import { PluginDocumentSettingPanel } from '@wordpress/edit-post';
import { compose } from '@wordpress/compose';
import { withSelect } from '@wordpress/data';
import ProtectedField from '../protected-field';

const screens = ['post', 'page'];

const StardustPanel = ({ postType }) => {
  // if ( !screens.includes(postType) ) { return null; }

  return (
    <PluginDocumentSettingPanel title="Stardust Subscribers" initialOpen="true">
      <ProtectedField />
    </PluginDocumentSettingPanel>
  )
};

export default compose([
  withSelect( ( select ) => {
    return {
      postType: select( 'core/editor' ).getCurrentPostType(),
    };
  }),
])(StardustPanel);
